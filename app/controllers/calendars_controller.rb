class CalendarsController < ApplicationController

  def index
    byebug
    calendar_system = CalendarServices.new(cal_params)
    calendar_params = CalendarServices.find_user_events
  end

  def show
    calendar_system = CalendarServices.new(cal_params)
    calendar_params = CalendarServices.get_event_details
  end

  def create
    byebug
    calendar_system = CalendarServices.new(cal_params)
    byebug
    calendar_params = CalendarServices.create_event
    @calendar = Calendar.create(calendar_params)
    @calendar.token_refreshed = calendar_system.token_refreshed
    json.response(@calendar, :created)
  end

  def update
    calendar_system = CalendarServices.new(cal_params)
    calendar_params = CalendarServices.update_event
    @calendar = Calendar.where(calendar_id: calendar_system.calendar_id).update(calendar_params)
    @calendar.token_refreshed = calendar_system.token_refreshed
    json.response(@calendar, :created)
  end

  def destroy
    calendar_system = CalendarServices.new(cal_params)
    calendar_params = CalendarServices.delete_event
    @calendar = Calendar.where(calendar_id: calendar_system.calendar_id).destroy
    @calendar.token_refreshed = calendar_system.token_refreshed
    json.response(@calendar, :created)
  end

  private
    def cal_params
      byebug
      params.permit(:email, :refresh_token, :oauth_token, :start_date, :end_date, :duration, :place, :summary, :oauth_expires_at, :user_id, :calendar_events)
    end
end
