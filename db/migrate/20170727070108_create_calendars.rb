class CreateCalendars < ActiveRecord::Migration[5.1]
  def change
    create_table :calendars do |t|
      t.string :user_id
      t.datetime :start_date
      t.datetime :end_date
      t.integer :duration
      t.string :place
      t.jsonb :person
      t.string :summary
      t.string :calendar_events

      t.timestamps
    end
  end
end
