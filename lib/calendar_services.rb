require 'google/apis'
require 'google/api_client/client_secrets'

class CalendarServices
  @@google_client_id = "254934277717-e001ih0ibdtlt44vtv4krg4agsl42ls4.apps.googleusercontent.com"
  @@google_client_secret = "D4LAdcRFhoOtJzIlWglMI2bT"

  def initialize(cal_params)
    byebug
    @cal_params = cal_params
    @refreshed_token = false
    @client = Google::APIClient::ClientSecrets.new(
      web: {
        client_id: @@google_client_id,
        client_secret: @@google_client_secret
      }
    )
    @client.to_authorization.redirect_uri = "urn:ietf:wg:auth:2.0:oob"
    if cal_params.oauth_expires_at < Time.now
      @client.to_authorization.update!(
        :additional_parameters => {
          "include_granted_scoped" => "true",
          "access_type" => "offline"
        }
      )
      byebug
      # @client.methods.sort
      @client.to_authorization.refresh_token = @cal_params.refresh_token
      request = @client.to_authorization.fetch_access_token!
      @cal_params.oauth_token = request.oauth_token
      @refreshed_token = true
    end
    @calendar_api = @client.discovered_api('calendar', 'v3')
  end

  def create_event
    byebug
    parameters = {
      summary: 'Google I/O 2016',
      location: '800 Howard St., San Francisco, CA 94103',
      description: 'A chance to hear more about Google\'s developer products.',
      start: {
        date_time: '2017-07-28T09:00:00-07:00',
        time_zone: 'America/Los_Angeles',
      },
      end: {
        date_time: '2017-07-29T17:00:00-07:00',
        time_zone: 'America/Los_Angeles',
      },
      recurrence: [
        'RRULE:FREQ=DAILY;COUNT=2'
      ],
      attendees: [
        {email: 'lpage@example.com'},
        {email: 'sbrin@example.com'},
      ],
      reminders: {
        use_default: false,
        overrides: [
          # Google::Apis::CalendarV3::EventReminder.new(reminder_method: "popup", minutes: 10),
          # Google::Apis::CalendarV3::EventReminder.new(reminder_method: "email", minutes: 24 * 60),
          {'reminder_method': 'popup', 'minutes': 10},
          {'reminder_method': 'email', 'minutes': 24 * 60}
        ]
      }
    }
    # Any 1 of them below will work. Try and detech which one of them.
    # @client.execute!(:api_method => @calendar_api.event.new, :parameters => parameters)
    # @calendar_api::Event.new(parameters)
  end

  def update_event

  end

  def delete_event

  end

  def get_event_details

  end


  def find_user_events

  end
end
